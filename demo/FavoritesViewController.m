//
//  FavoritesViewController.m
//  demo
//
//  Created by Denis Chepil on 10.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "FavoritesViewController.h"
#import "DBTools.h"
#import "CityTableViewCell.h"
#import "Favorites.h"
#import "Cities.h"

@interface FavoritesViewController () <UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, weak) NSFetchedResultsController *frc;
@property (nonatomic, weak) IBOutlet UITableView *tblView;

@property (weak, nonatomic) IBOutlet UIImageView *emptyDatasetImage;


@end

@implementation FavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSError *error = nil;
    _frc = [[DBTools sharedInstance] favoritesFetchController];
    _frc.delegate = self;
    [_frc performFetch:&error];
    if (error) {
        NSLog(@"error, while fetching favorites: %@", error);
    }
    
    self.tblView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    long c = [[_frc fetchedObjects] count];
    [_emptyDatasetImage setHidden:(c>0)];
    return c;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    id <NSFetchedResultsSectionInfo> sectionInfo = [[_frc sections] objectAtIndex:indexPath.section];
    
    Favorites *favorite = (Favorites*)[[sectionInfo objects] objectAtIndex:indexPath.row];
    Cities *city = (Cities*)favorite.citiesRel;
    
    [cell.asteriskBtn setHighlighted:TRUE];
    [cell.asteriskBtn setUserData:city];
//    [cell.asteriskBtn addTarget:self action:@selector(asteriskBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.title setText:city.name];
    
    return cell;
}

#pragma mark -
#pragma mark TableView animations

-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [_tblView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [_tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                            withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                            withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [_tblView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [_tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                            withRowAnimation:UITableViewRowAnimationFade];
            [_tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                            withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [UIView animateWithDuration:0.5 animations:^(){
        [_tblView endUpdates];
    }];
}

#pragma mark -
#pragma mark User Actions
/*
-(void)asteriskBtnPress:(id)sender {
    Cities *city = [(AsteriskButton*)sender userData];
    if (city && city.name) {
        [[DBTools sharedInstance] setupFavoritesForCity:city];
    }
}*/

@end
