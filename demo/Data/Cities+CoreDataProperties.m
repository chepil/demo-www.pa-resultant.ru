//
//  Cities+CoreDataProperties.m
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Cities+CoreDataProperties.h"

@implementation Cities (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic imageLink;
@dynamic countryId;
@dynamic countryRel;
@dynamic favoritesRel;

@end
