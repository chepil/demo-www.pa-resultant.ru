//
//  Countries+CoreDataProperties.h
//  demo
//
//  Created by Denis Chepil on 09.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Countries.h"

NS_ASSUME_NONNULL_BEGIN

@interface Countries (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *imageLink;
@property (nullable, nonatomic, retain) NSNumber *expanded;
@property (nullable, nonatomic, retain) NSSet<Cities *> *citiesRel;

@end

@interface Countries (CoreDataGeneratedAccessors)

- (void)addCitiesRelObject:(Cities *)value;
- (void)removeCitiesRelObject:(Cities *)value;
- (void)addCitiesRel:(NSSet<Cities *> *)values;
- (void)removeCitiesRel:(NSSet<Cities *> *)values;

@end

NS_ASSUME_NONNULL_END
