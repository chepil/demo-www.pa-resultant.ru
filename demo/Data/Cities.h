//
//  Cities.h
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Favorites;

NS_ASSUME_NONNULL_BEGIN

@interface Cities : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Cities+CoreDataProperties.h"
