//
//  Favorites+CoreDataProperties.m
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Favorites+CoreDataProperties.h"

@implementation Favorites (CoreDataProperties)

@dynamic citiesRel;

@end
