//
//  Countries+CoreDataProperties.m
//  demo
//
//  Created by Denis Chepil on 09.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Countries+CoreDataProperties.h"

@implementation Countries (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic imageLink;
@dynamic expanded;
@dynamic citiesRel;

@end
