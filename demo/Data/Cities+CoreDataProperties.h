//
//  Cities+CoreDataProperties.h
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Cities.h"
#import "Countries.h"

NS_ASSUME_NONNULL_BEGIN

@interface Cities (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *imageLink;
@property (nullable, nonatomic, retain) NSNumber *countryId;
@property (nullable, nonatomic, retain) Countries *countryRel;
@property (nullable, nonatomic, retain) Favorites *favoritesRel;

@end

NS_ASSUME_NONNULL_END
