//
//  CityTableViewCell.h
//  demo
//
//  Created by Denis Chepil on 09.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cities.h"
#import "AsteriskButton.h"

@interface CityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet AsteriskButton *asteriskBtn;

@end
