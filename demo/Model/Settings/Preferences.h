//
//  Preferences.h
//  demo
//
//  Created by Denis Chepil on 10.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cities.h"

@interface Preferences : NSObject

@property (nonatomic, strong, getter=getSelectedCity, setter=setSelectedCity:) Cities * _Nullable selectedCity;

+(id _Nonnull)sharedInstance;

@end
