//
//  Preferences.m
//  demo
//
//  Created by Denis Chepil on 10.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "Preferences.h"

#import "DBTools.h"

#define selectedCityKey @"selectedCity"

@interface Preferences()

@property (nonatomic, strong) NSUserDefaults *def;

@end

@implementation Preferences

static Preferences* sPreferences = nil;

@synthesize selectedCity = _selectedCity;

+(id)sharedInstance {
    static dispatch_once_t predicate;
    dispatch_once( &predicate, ^{
        sPreferences = [ [ self alloc ] init ];
        [sPreferences initDefaults];
    } );
    return sPreferences;
}

-(void)initDefaults {
    _def = [NSUserDefaults standardUserDefaults];
}

-(void)setSelectedCity:(Cities*)inCity {
    _selectedCity = inCity;
    [_def setInteger:[inCity.id integerValue] forKey:selectedCityKey];
    [_def synchronize];
}

-(Cities*)getSelectedCity {
    if (_selectedCity)
        return _selectedCity;
    
    if ([_def objectForKey:selectedCityKey]) {
        NSInteger cityId = [_def integerForKey:selectedCityKey];
        Cities *city = [[DBTools sharedInstance] lookingCityById:cityId];
        if (city)
            _selectedCity = city;
        return _selectedCity;
    }
    return nil;
}

@end
