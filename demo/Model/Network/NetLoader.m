 //
//  NetLoader.m
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "NetLoader.h"

@interface NetLoader() <NSURLSessionDelegate, NSURLSessionTaskDelegate>
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) _Nullable id json;
@end

@implementation NetLoader

@synthesize delegate;

-(id)init {
    if (self = [super init]) {
        _session = [NSURLSession sharedSession];
    }
    return self;
}

-(void)loadDataFromUrl:(nonnull NSString*)stringUrl {
    //if (!stringUrl || stringUrl.length==0) //TODO need check url
    //    return;
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSURLSessionDataTask *dataTask = [_session dataTaskWithURL:url
                                             completionHandler:^(NSData *data,
                                                                 NSURLResponse *response,
                                                                 NSError *error)
    {
        if (!error) {
            _json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (_json)
                if (self.delegate && [self.delegate respondsToSelector:@selector(didLoadJson:)]) {
                    [self.delegate didLoadJson:_json];
                }
        }
    }];
    [dataTask resume];
}

-(void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
#ifdef DEBUG
    NSLog(@"%@", error);
#endif
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCompleteWithError:)]) {
        [self.delegate didCompleteWithError:error];
    }
}

@end
