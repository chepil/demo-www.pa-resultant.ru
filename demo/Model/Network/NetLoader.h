//
//  NetLoader.h
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetLoader;

@protocol NetLoaderDelegate <NSObject>

@optional
-(void)didLoadJson:(_Nullable id)jsonData;
-(void)didCompleteWithError:(NSError* _Nullable)error;

@end

@interface NetLoader : NSObject

@property _Nullable id <NetLoaderDelegate> delegate;

-(void)loadDataFromUrl:(nonnull NSString *)stringUrl;

@end
