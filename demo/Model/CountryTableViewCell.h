//
//  CountryTableViewCell.h
//  demo
//
//  Created by Denis Chepil on 09.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *countryButton;
@property (weak, nonatomic) IBOutlet UIImageView *stateImage;


@end
