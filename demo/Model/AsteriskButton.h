//
//  AsteriskButton.h
//  demo
//
//  Created by Denis Chepil on 09.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsteriskButton : UIButton {
    id userData;
}

@property (nonatomic, readwrite, retain) id userData;

@end
