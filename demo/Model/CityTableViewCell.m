//
//  CityTableViewCell.m
//  demo
//
//  Created by Denis Chepil on 09.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "CityTableViewCell.h"
#import "DBTools.h"

@implementation CityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)asteriskButtonPress:(id)sender {
    Cities *city = [(AsteriskButton*)sender userData];
    if (city && city.name) {
        [[DBTools sharedInstance] setupFavoritesForCity:city];
    }
}

@end
