//
//  JsonDataParser.m
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "JsonDataParser.h"
#import "Countries.h"
#import "Cities.h"
#import "DBTools.h"

@implementation JsonDataParser

-(instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark -
#pragma mark LoadData

//Внимание! База данных создается в памяти. SQLite и XML базы не используются.
//Так сделано для упрощения работы демо примера

-(void)loadData:(id)jsonData {
    //тут нужно много обработчиков ошибок писать, или воспользоваться готовым фреймворком.
    //мне лень делать первое и нельзя делать второе. поэтому пишем говнокод ( для скорости )
    
    DBTools *db = [DBTools sharedInstance];
    //TODO проверить, если уже есть записи в базе данных, удалить и по новой залить
    
    if ([jsonData objectForKey:@"Result"]) {
        for (NSDictionary *countryDict in [jsonData objectForKey:@"Result"]) {
            
            Countries *country = [NSEntityDescription insertNewObjectForEntityForName:@"Countries" inManagedObjectContext:db.managedObjectContext];
            
            if ([countryDict objectForKey:@"Name"])
                country.name = [countryDict valueForKey:@"Name"];
            if ([countryDict objectForKey:@"Id"])
                country.id = [countryDict valueForKey:@"Id"];
            if ([countryDict objectForKey:@"ImageLink"])
                country.imageLink = [countryDict valueForKey:@"ImageLink"];
            
            country.expanded = [NSNumber numberWithBool:NO];
            
            //создадим для этой страны Cities
            for (NSDictionary *citiesDict in [countryDict objectForKey:@"Cities"]) {
                
                //for (NSDictionary *artistsDict in [citiesDict objectForKey:@"Artists"]) {
                
                    Cities *city = [NSEntityDescription insertNewObjectForEntityForName:@"Cities" inManagedObjectContext:db.managedObjectContext];
                    
                    if ([citiesDict objectForKey:@"Name"])
                        city.name = [citiesDict valueForKey:@"Name"];
                    if ([citiesDict objectForKey:@"Id"])
                        city.id = [citiesDict valueForKey:@"Id"];
                    if ([citiesDict objectForKey:@"ImageLink"] && (![[citiesDict objectForKey:@"ImageLink"] isEqual:[NSNull null]]))
                        city.imageLink = [citiesDict valueForKey:@"ImageLink"];
                    if ([citiesDict objectForKey:@"CountryId"])
                        city.countryId = [citiesDict valueForKey:@"CountryId"];
                    city.countryRel = country;
                    
                    [country addCitiesRelObject:city];
                //}
            }
        }
    }
    
    //тут нужно написать обработчик ошибок для базы данных, но мне лень ( верней некогда )
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    dispatch_time_t timeout = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC));
    dispatch_async(dispatch_get_main_queue(), ^() {
        NSError *error = nil;
        [db.managedObjectContext save:&error];
        if (error)
            NSLog(@"error: %@", error);
        dispatch_semaphore_signal(semaphore);
    });
    dispatch_semaphore_wait(semaphore, timeout);
}


@end
