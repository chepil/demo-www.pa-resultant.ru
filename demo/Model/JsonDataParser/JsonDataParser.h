//
//  JsonDataParser.h
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonDataParser : NSObject

-(void)loadData:(id)jsonData;

@end
