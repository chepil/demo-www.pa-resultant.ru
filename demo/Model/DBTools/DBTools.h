//
//  DBTools.h
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Cities.h"

@interface DBTools : NSObject <NSFetchedResultsControllerDelegate>

@property (readonly, strong, nonatomic) NSManagedObjectContext * _Nonnull managedObjectContext;

@property (strong, nonatomic) NSFetchedResultsController * _Nullable citiesByCountriesFetchController;
@property (nonatomic, strong) NSFetchedResultsController * _Nullable favoritesFetchController;


+(id _Nonnull)sharedInstance;
-(void)saveContext;
//-(void)removeAllRecords;
-(void)setupFavoritesForCity:(Cities* _Nonnull)city;
-( Cities* _Nullable )lookingCityById:(NSInteger)cityId;
    
@end
