//
//  DBTools.m
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "DBTools.h"
#import "Favorites.h"

@interface DBTools()


@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

@implementation DBTools

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize citiesByCountriesFetchController;
@synthesize favoritesFetchController;

static DBTools* sDBTools = nil;

+(id)sharedInstance {
    static dispatch_once_t predicate;
    dispatch_once( &predicate, ^{
        sDBTools = [ [ self alloc ] init ];
    } );
    return sDBTools;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                              NSInferMappingModelAutomaticallyOption:@YES//,
                              //NSSQLitePragmasOption: @{@"journal_mode": @"delete"}
                              };
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType
                                                   configuration:nil
                                                             URL:nil
                                                         options:options
                                                           error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"INIT_DB_ERROR" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];//NSMainQueueConcurrencyType
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges]) {
            
            [managedObjectContext performBlockAndWait:^{
                NSError *error = nil;
                [managedObjectContext save:&error];
                if (error)
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                
                for (NSManagedObject *object in [managedObjectContext registeredObjects]) {
                    [managedObjectContext refreshObject:object mergeChanges:YES];
                }
            }];
        }
        
    }
}

- (NSFetchedResultsController *)citiesByCountriesFetchController {
    if (!self.managedObjectContext) return nil;
    
    if (citiesByCountriesFetchController != nil) {
        return citiesByCountriesFetchController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Cities" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    
    NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:@"countryRel.name" ascending:YES];
    NSSortDescriptor *sort2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort1,sort2, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:@"countryRel.name"
                                                   cacheName:nil];
    self.citiesByCountriesFetchController = theFetchedResultsController;
    citiesByCountriesFetchController.delegate = self;
    
    return self.citiesByCountriesFetchController;
    
}

-(void)setupFavoritesForCity:(Cities*)city {
    //check if we have favorites forever...
    BOOL isFavorite = (city.favoritesRel != nil);
    if (!isFavorite) {
        Favorites *favorites = [NSEntityDescription insertNewObjectForEntityForName:@"Favorites" inManagedObjectContext:_managedObjectContext];
        favorites.citiesRel = city;
        city.favoritesRel = favorites;
    } else {
        Favorites *favorites = city.favoritesRel;
        [_managedObjectContext deleteObject:favorites];
        city.favoritesRel = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^() {
        NSError *error = nil;
        [_managedObjectContext save:&error];
        if (error)
            NSLog(@"error: %@", error);
    });
}

-( Cities* _Nullable )lookingCityById:(NSInteger)cityId {
    Cities *city = nil;
    NSError *error = nil;
    NSFetchedResultsController *frc = [self citiesByIdFetchController:cityId];
    if (![frc performFetch:&error]) {
        NSLog(@"Unresolved error %@", error);
        return nil;
    }
    if ([frc fetchedObjects] && ([[frc fetchedObjects] count]>0))
        city = [[frc fetchedObjects] objectAtIndex:0];
    else
        NSLog(@"city not found");
    
    return city;
}

- (NSFetchedResultsController *)citiesByIdFetchController:(NSInteger)cityId {
    if (!self.managedObjectContext) return nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Cities" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:1];
    
    
    NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort1, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    //predicate
    NSString *str = [NSString stringWithFormat:@"( id == %ld )",cityId];
    NSPredicate *predicateTemplate = [NSPredicate predicateWithFormat:str];
    [fetchRequest setPredicate:predicateTemplate];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    return theFetchedResultsController;
    
}

- (NSFetchedResultsController *)favoritesFetchController {
    if (!self.managedObjectContext) return nil;
    
    if (favoritesFetchController != nil) {
        return favoritesFetchController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Favorites" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    
    NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:@"citiesRel.name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sort1, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    self.favoritesFetchController = theFetchedResultsController;
    favoritesFetchController.delegate = self;
    
    return self.favoritesFetchController;
    
}


@end
