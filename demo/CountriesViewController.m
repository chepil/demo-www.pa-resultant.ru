//
//  CountriesViewController.m
//  demo
//
//  Created by Denis Chepil on 08.06.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "CountriesViewController.h"
#import "NetLoader.h"
#import "JsonDataParser.h"
#import "DBTools.h"
#import "Cities.h"
#import "Countries.h"
#import "CountryTableViewCell.h"
#import "CityTableViewCell.h"
#import "Preferences.h"


#define stringUrl @"https://atw-backend.azurewebsites.net/api/countries"

@interface CountriesViewController () <NetLoaderDelegate, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, weak) NetLoader* netLoader;
@property (nonatomic, strong) IBOutlet UITableView *tblView;

@property (nonatomic, strong) NSArray<id<NSFetchedResultsSectionInfo>> *sections;

@property (nonatomic, strong) Cities * _Nullable selectedCity;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation CountriesViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [_activityIndicator startAnimating];
    NetLoader *netLoader = [NetLoader new];
    netLoader.delegate = self;
    [netLoader loadDataFromUrl:stringUrl];
    //top padding to the first row in table
    self.tblView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark NetLoader Delegate

-(void)didLoadJson:(_Nullable id)jsonData {
#ifdef DEBUG
    //NSLog(@"loaded data: %@", jsonData);
#endif
    [[JsonDataParser new] loadData:jsonData];
    [self fetchData];
    
    dispatch_async(dispatch_get_main_queue(), ^(){
        _selectedCity = [[Preferences sharedInstance] getSelectedCity];
        if (_selectedCity)
            _selectedCity.countryRel.expanded = [NSNumber numberWithBool:YES];
        
        [_tblView reloadData];
        [_activityIndicator stopAnimating];
    });
}

-(void)didCompleteWithError:(NSError* _Nullable)error {
#ifdef DEBUG
    NSLog(@"didCompleteWithError: %@", error);
    [_activityIndicator stopAnimating];
#endif
}

#pragma mark -
#pragma mark DataSource Delegate

-(void)fetchData {
    DBTools *db = [DBTools sharedInstance];
    [db setCitiesByCountriesFetchController:nil];
    NSError *error = nil;
    if (![[db citiesByCountriesFetchController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        return;
    }
    [db citiesByCountriesFetchController].delegate = self;
    _sections = [[db citiesByCountriesFetchController] sections];
}

#pragma mark -
#pragma mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rows = 0;
    if ([_sections count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [_sections objectAtIndex:section];
        rows = [sectionInfo numberOfObjects];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    id <NSFetchedResultsSectionInfo> sectionInfo = [_sections objectAtIndex:indexPath.section];
    
    Cities *city = (Cities*)[[sectionInfo objects] objectAtIndex:indexPath.row];

    [cell.asteriskBtn setSelected:(city.favoritesRel!=nil)];
    [cell.asteriskBtn setUserData:city];
//    [cell.asteriskBtn addTarget:self action:@selector(asteriskBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.title setText:city.name];
    
    BOOL selected = [city isEqual:_selectedCity];
    [cell setSelected:selected];
    
    if (selected) {
        [tableView selectRowAtIndexPath:indexPath
                               animated:TRUE
                         scrollPosition:UITableViewScrollPositionNone
        ];
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [_sections objectAtIndex:section];
    if ([[sectionInfo objects] count] > 0) {
        Cities *city = (Cities*)[[sectionInfo objects] objectAtIndex:0];
        Countries *country = city.countryRel;
        
        static NSString *CellIdentifier = @"countryCell";
        
        CountryTableViewCell *sectionHeaderCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];//
        [sectionHeaderCell.contentView setBackgroundColor:[UIColor whiteColor]];
        
        [sectionHeaderCell.title setText:country.name];
        [sectionHeaderCell.countryButton setTag:section];
        [sectionHeaderCell.countryButton addTarget:self action:@selector(countryTap:) forControlEvents:UIControlEventTouchUpInside];
        
        BOOL expanded = [country.expanded boolValue];
        
        [sectionHeaderCell.stateImage setImage:[UIImage imageNamed:(expanded)?@"earth":@"city"]];
        [sectionHeaderCell.stateImage setTintColor:(expanded)?[UIColor greenColor]:[UIColor lightGrayColor]];
        return sectionHeaderCell.contentView;
    }
    return nil;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id <NSFetchedResultsSectionInfo> sectionInfo = [_sections objectAtIndex:indexPath.section];
    if ([[sectionInfo objects] count] > 0) {
        Cities *city = (Cities*)[[sectionInfo objects] objectAtIndex:0];
        return ([city.countryRel.expanded boolValue])?44.0f:0.0f;
    }
    return 0;
}

#pragma mark -
#pragma mark TableView animations

-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [_tblView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [_tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [_tblView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [_tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [_tblView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [UIView animateWithDuration:0.5 animations:^(){
        [_tblView endUpdates];
    }];
}

#pragma mark -
#pragma mark User Actions
/*
-(void)asteriskBtnPress:(id)sender {
    Cities *city = [(AsteriskButton*)sender userData];
    if (city && city.name) {
        [[DBTools sharedInstance] setupFavoritesForCity:city];
    }
}
*/
-(void)countryTap:(id)sender {
    long countryIndex = [(UIButton*)sender tag];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [_sections objectAtIndex:countryIndex];
    if ([[sectionInfo objects] count] > 0) {
        Cities *city = (Cities*)[[sectionInfo objects] objectAtIndex:0];
        Countries *country = city.countryRel;
        dispatch_async(dispatch_get_main_queue(),^(){
            BOOL expanded = [country.expanded boolValue];
            country.expanded = [NSNumber numberWithBool:!expanded];
            NSError *error = nil;
            if (![country.managedObjectContext save:&error]) {
                NSLog(@"error: %@", error);
            }
            NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:countryIndex];
            [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:60 initialSpringVelocity:30 options:UIViewAnimationOptionTransitionNone animations:^(){
                [_tblView beginUpdates];
                [_tblView endUpdates];
                
            } completion:^(BOOL finished) {
                _selectedCity = [[Preferences sharedInstance] getSelectedCity];
                [_tblView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            }];
        });
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id <NSFetchedResultsSectionInfo> sectionInfo = [_sections objectAtIndex:indexPath.section];
    if ([[sectionInfo objects] count] > 0) {
        Cities *city = (Cities*)[[sectionInfo objects] objectAtIndex:indexPath.row];
        [[Preferences sharedInstance] setSelectedCity:city];
    }
}

@end
